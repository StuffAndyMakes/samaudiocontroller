///
/// @mainpage	SAMAudioController
///
/// @details	COBS Packet Test on Arduino Mega 2560
/// @n
/// @n
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Andy
/// @author		StuffAndyMakes.com
/// @date		6/13/15 8:21 AM
/// @version	<#version#>
///
/// @copyright	(c) Andy, 2015
/// @copyright	CC = BY SA NC
///
/// @see		ReadMe.txt for references
///


///
/// @file		SAMAudioController.cpp
/// @brief		Main sketch
///
/// @details	<#details#>
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Andy
/// @author		StuffAndyMakes.com
/// @date		6/13/15 8:21 AM
/// @version	<#version#>
///
/// @copyright	(c) Andy, 2015
/// @copyright	CC = BY SA NC
///
/// @see		ReadMe.txt for references
/// @n
///


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(ROBOTIS) // Robotis specific
#include "libpandora_types.h"
#include "pandora.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(RFDUINO) // RFduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "application.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE


#include "SerialHandler.h"
#include <Wire.h>
#include <SPI.h>
#include "Adafruit_VS1053.h"
#include <SD.h>


#define DEFAULT_AMP_VOLUME 50
#define DEFAULT_LEFT_VOLUME 15
#define DEFAULT_RIGHT_VOLUME 15

//SerialHandler comms( &Serial, '\n' );
#define SERIAL_BUF_SIZE (32)

#define BREAKOUT_RESET  ((uint8_t)9)    // "RESET" or "RST" (on my board) VS1053 reset pin (output)
#define BREAKOUT_CS     ((uint8_t)10)   // "CS" VS1053 chip select pin (output)
#define BREAKOUT_DCS    ((uint8_t)8)    // "XDCS" or "DCS" VS1053 Data/command select pin (output)
#define CARDCS          ((uint8_t)4)    // "SDCS" Card chip select pin
#define DREQ            ((uint8_t)5)    // VS1053 Data request, ideally an Interrupt pin

Adafruit_VS1053_FilePlayer musicPlayer = Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);

#define MAX9744_I2CADDR 0x4B
#define AMP_SHUTDOWN_PIN A3

#define LOOP_CONTINUOUSLY (-1)

boolean isShutdown = false;
uint8_t ampVolume = DEFAULT_AMP_VOLUME;
uint8_t leftVolume = DEFAULT_LEFT_VOLUME;
uint8_t rightVolume = DEFAULT_RIGHT_VOLUME;
boolean playing = false;
boolean looping = false;
long loopsRemaining = LOOP_CONTINUOUSLY; // default to infinite looping
char nowPlaying[13] = "";
File currentTrack;
char serialBuffer[SERIAL_BUF_SIZE] = "";
uint8_t bufferPos = 0;

#define CMD_SHUTDOWN '!'
#define CMD_INIT 'i'
#define CMD_STATUS 'u'
#define CMD_AMP_VOL 'v'
#define CMD_LEFT_VOL 'f'
#define CMD_RIGHT_VOL 'r'
#define CMD_PAUSE 'p'
#define CMD_STOP 's'
#define CMD_TRACK 't'
#define CMD_LOOP 'l'
#define CMD_ONCE 'o'
#define CMD_META 'm'
#define CMD_TEST 'e'


void ampOff() {
    digitalWrite( AMP_SHUTDOWN_PIN, LOW );
}

void ampOn() {
    digitalWrite( AMP_SHUTDOWN_PIN, HIGH );
}

boolean setVolume(uint8_t v) {
    // cant be higher than 63 or lower than 0
    if (v > 63) v = 63;
    if (v < 0) v = 0;
    Wire.begin(); // seems to be needed
    Wire.beginTransmission(MAX9744_I2CADDR);
    Wire.write(v);
    uint8_t r = Wire.endTransmission(true);
    if (r == 0) {
        ampVolume = v;
    }
    /*  switch( r ) {
     case 1:
     Serial.println( "ERR (Data too long)" );
     return false;
     break;
     case 2:
     Serial.println( "ERR (NACK on transmit of address)" );
     return false;
     break;
     case 3:
     Serial.println( "ERR (NACK on transmit of data)" );
     return false;
     break;
     case 4:
     Serial.println( "ERR (Some other error)" );
     return false;
     break;
     }
     Serial.println( "OK" );*/
    return r;
}

void setup() {
    
    Serial.begin(115200);
    Serial.println(F("I StuffAndyMakes.com Audio Controller"));
    
    pinMode(AMP_SHUTDOWN_PIN, OUTPUT);
    digitalWrite(AMP_SHUTDOWN_PIN, HIGH); // amp on initially
    Wire.begin();
    uint8_t r = setVolume(DEFAULT_AMP_VOLUME);
    if (r != 0) {
        Serial.println(String(F("E Failed to set volume")) + " (" + String(r, DEC) + ")");
    }
    
    if (!musicPlayer.begin()) {
        Serial.println(F("E MP3 player didn't start"));
    } else {
        SD.begin(CARDCS); // initialize the SD card
        // Set volume for left, right channels. lower numbers == louder volume!
        musicPlayer.setVolume(DEFAULT_LEFT_VOLUME, DEFAULT_RIGHT_VOLUME);
    }

    pinMode(7, OUTPUT); // test LED
    digitalWrite(7, LOW);
}

//void startPlaying() {
//    musicPlayer.currentTrack = SD.open(nowPlaying);
//    if (musicPlayer.currentTrack) {
//        musicPlayer.playingMusic = true;
//        if (looping) {
//            if (loopsRemaining != -1) {
//                Serial.print(F("I Playing "));
//                Serial.print(nowPlaying);
//                Serial.print(F(" "));
//                Serial.print(loopsRemaining, DEC);
//                Serial.println(F(" times"));
//            }
//        } else {
//            Serial.print(F("I Playing "));
//            Serial.println(nowPlaying);
//        }
//    }
//}

void stopPlayer(boolean reset) {
    if (playing) {
        playing = false;
        Serial.println(String(F("I Stop ")) + String(nowPlaying));
        if (reset) {
            currentTrack.close();
            musicPlayer.softReset();
            nowPlaying[0] = 0;
        }
    }
}

void playTrack(char *t) {
    if (playing) {
        playing = false;
        currentTrack.close();
        Serial.println(String(F("I Stopped ")) + String(nowPlaying));
    }
    musicPlayer.softReset();
    strcpy(nowPlaying, t);
    strcat(nowPlaying, ".mp3");
    if (SD.exists(nowPlaying)) {
        currentTrack = SD.open(nowPlaying);
        if (currentTrack) {
            playing = true;
            if (looping) {
                Serial.print(F("I Playing "));
                Serial.print(nowPlaying);
                Serial.print(F(" "));
                if (loopsRemaining == LOOP_CONTINUOUSLY) {
                    // loop until stopped
                    Serial.print(F("until stopped"));
                } else {
                    // loop X times, start countdown
                    Serial.print(loopsRemaining, DEC);
                    Serial.println(F(" times"));
                }
            } else {
                Serial.print(F("I Playing "));
                Serial.println(nowPlaying);
            }
        } else {
            Serial.print(F("E Opening "));
            Serial.println(nowPlaying);
            nowPlaying[0] = 0;
        }
    } else {
        Serial.println(String(F("E Missing ")) + String(nowPlaying));
        nowPlaying[0] = 0;
    }
}

void playAgain() {
    if (currentTrack) {
        currentTrack.seek(0);
    }
}

long getValueFromCommand(char *line) {
    if (strlen(line) > 1) {
        char *endp;
        return strtol(line + 1, &endp, 10);
    }
    return -1;
}

void interpretCommand(char *line) {
    if (line[0] == 0) {
        Serial.println(F("E Empty command"));
        return;
    }
    char command = line[0];
    char *valueString;
    long value = 0;
    switch (command) {
        case CMD_SHUTDOWN:
            ampOff();
            musicPlayer.stopPlaying();
            isShutdown = true;
            Serial.println(F("I Audio system shut down"));
            break;
            
        case CMD_INIT:
            isShutdown = false;
            ampOn();
            musicPlayer.stopPlaying();
            Serial.println(F("I Audio system initialized"));
            break;
            
        case CMD_STATUS:
            Serial.print(String(F("I ")));
            if (isShutdown) {
                Serial.print(F("Shutdown, "));
            } else {
                if (nowPlaying[0] != 0) {
                    if (playing) {
                        Serial.print(F("Playing "));
                    } else {
                        Serial.print(F("Paused "));
                    }
                    Serial.print(nowPlaying);
                    Serial.print(F(", "));
                } else {
                    Serial.print(F("Running, Stopped, "));
                }
            }
            Serial.print(F("AmpVol "));
            Serial.print(ampVolume, DEC);
            Serial.print(F(", LVol "));
            Serial.print(leftVolume, DEC);
            Serial.print(F(", RVol "));
            Serial.print(rightVolume, DEC);
            Serial.println(" ");
            break;
            
        case CMD_AMP_VOL:
            value = getValueFromCommand(line);
            if (value > -1) {
                setVolume((uint8_t)value);
                Serial.println(String(F("I Amp volume now ")) + String(ampVolume, DEC));
            } else {
                Serial.println(String(F("E No amp volume value given")));
            }
            break;
            
        case CMD_LEFT_VOL:
            value = getValueFromCommand(line);
            if (value > -1) {
                leftVolume = (uint8_t)value;
                musicPlayer.setVolume(leftVolume, rightVolume);
                Serial.println(String(F("I Left volume now ")) + String(leftVolume, DEC));
            } else {
                Serial.println(String(F("E No left volume value given")));
            }
            break;
            
        case CMD_RIGHT_VOL:
            value = getValueFromCommand(line);
            if (value > -1) {
                rightVolume = (uint8_t)value;
                musicPlayer.setVolume(leftVolume, rightVolume);
                Serial.println(String(F("I Right volume now ")) + String(rightVolume, DEC));
            } else {
                Serial.println(F("E No right volume value given"));
            }
            break;
            
        case CMD_PAUSE:
            if (!playing && nowPlaying[0] != 0) {
                playing = true;
                Serial.println(F("I Audio resumed"));
            } else {
                playing = false;
                Serial.println(F("I Audio paused"));
            }
            break;
            
        case CMD_STOP:
            stopPlayer(true);
            nowPlaying[0] = 0; // blank the current track name
            break;
            
        case CMD_LOOP:
            if (line[0] != 0) {
                // looking for how many times to loop sound
                loopsRemaining = getValueFromCommand(line);
            } else {
                loopsRemaining = LOOP_CONTINUOUSLY;
            }
            looping = true;
            Serial.print(F("I Loop "));
            if (loopsRemaining == LOOP_CONTINUOUSLY) {
                Serial.println(F("forever"));
            } else {
                Serial.println(String(loopsRemaining, DEC) + String(F(" times")));
            }
            break;
            
        case CMD_ONCE:
            looping = false;
            Serial.println(F("I Play audio once"));
            break;

        case CMD_META: {
                static char fn[] = "meta.txt";
                if (SD.exists(fn)) {
                    Serial.println(F("I Meta:"));
                    File metaFile = SD.open(fn);
                    if (metaFile) {
                        while (metaFile.available()) {
                            Serial.write(metaFile.read());
                        }
                        metaFile.close();
                    } else {
                        Serial.println(F("E Error opening meta.txt"));
                    }
                } else {
                    Serial.println(F("E Missing meta.txt"));
                }
            }
            break;
        
        case CMD_TEST:
            value = getValueFromCommand(line);
            if (value > -1) {
                Serial.print(F("I Running sine test... "));
                musicPlayer.sineTest((uint8_t)value, 1000);
                Serial.println(F("OK"));
            }
            break;
            
        default:
            break;
    }
}

void loop() {

    // feed player buffer if it needs it (instead of using interrupts)
    if (playing && nowPlaying[0] != 0) {
        while (digitalRead(DREQ)) {
            digitalWrite(7, HIGH);
            int bytesread = currentTrack.read(musicPlayer.mp3buffer, VS1053_DATABUFFERLEN);
            if (bytesread == 0) {
                // end of file, what next?
                if (looping) {
                    digitalWrite(7, LOW);
                    if (loopsRemaining == LOOP_CONTINUOUSLY) {
                        // looping until stopped (infinite)
                        playAgain();
                    } else {
                        if (--loopsRemaining == 0) {
                            looping = false;
                        } else {
                            // start feeding process over again!
                            playAgain();
                            Serial.print(String(F("I Playing ")));
                            Serial.print(String(nowPlaying));
                            Serial.print(F(" "));
                            Serial.print(loopsRemaining, DEC);
                            Serial.println(F(" more times"));
                        }
                    }
                } else {
                    // not gonna loop, so wrap it up!
                    stopPlayer(false);
                    digitalWrite(7, LOW);
                    break;
                }
            } else {
                musicPlayer.playData(musicPlayer.mp3buffer, bytesread);
            }
            digitalWrite(7, LOW);
        }
    }

    if (Serial.available()) {
        char c = Serial.read();
        if (c == '\n') {
            // we've received a complete line, so figure out what to do with it
            if (isShutdown) {
                Serial.println(F("E System is shutdown"));
            } else {
                serialBuffer[bufferPos] = 0; // add null-terminator
                if (serialBuffer[0] >= '0' && serialBuffer[0] <= '9') {
                    // command entered was a number (request to play a track)
                    playTrack(serialBuffer);
                } else {
                    interpretCommand(serialBuffer);
                }
            }
            // start buffer over
            bufferPos = 0;
        } else {
            // first, discard any no-no chars
            if (c != '\r') {
                // don't allow overflow
                if (bufferPos < (SERIAL_BUF_SIZE - 1)) {
                    // stuff this character into buffer
                    serialBuffer[bufferPos++] = c;
                } else {
                    Serial.println(F("E Too many characters"));
                }
            }
        }
    }

}

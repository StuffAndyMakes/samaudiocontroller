#include "SerialHandler.h"


SerialHandler::SerialHandler( HardwareSerial *s, char t ) {
  serialPort = s;
  lineTerminator = t;
  timeoutTime = 0;
  timeoutDelay = 1000;
  timedOut = false;
  line = "";
  buffer = "";
  gotOne = false;
}

void SerialHandler::setTimeout(unsigned long t) {
  timeoutDelay = t;
}

void SerialHandler::clearTimeout() {
  timedOut = false;
}

void SerialHandler::loop() {
  if (serialPort->available() > 0) {
    char c = serialPort->read();
    if (c == lineTerminator) {
      buffer.trim();
      line = buffer;
      buffer = "";
      gotOne = true;
    } else {
      buffer += String( c );
    }
    // reset timeout stopwatch
    timeoutTime = millis() + timeoutDelay;
  } else if (millis() > timeoutTime) {
    timeoutTime = millis() + timeoutDelay;
    timedOut = true;
  }
}

boolean SerialHandler::gotLine() {
  return gotOne;
}

boolean SerialHandler::isTimedout() {
  return timedOut;
}

String SerialHandler::getLine() {
  gotOne = false;
  return line;
}

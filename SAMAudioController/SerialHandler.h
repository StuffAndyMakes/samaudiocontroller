#include <Arduino.h>

#ifndef SerialHandler_h
#define SerialHandler_h


class SerialHandler {
  
private:
    HardwareSerial *serialPort;
    char lineTerminator;
    String buffer, line;
    boolean gotOne;
    unsigned long timeoutTime, timeoutDelay;
    boolean timedOut;
    
public:
    SerialHandler( HardwareSerial *s, char t );
    void
        setTimeout( unsigned long t ),
        clearTimeout(),
        loop();
    boolean
        gotLine(),
        isTimedout();
    String getLine();

};

#endif
